package com.theromman.aggregators

import com.theromman.model.ProductInfo
import com.theromman.model.ProductInfoExtended

interface ProductPurchasingsAggregator {
    fun aggregate(products: List<String>, productsMappings: Map<String, ProductInfo>): Iterable<ProductInfoExtended>
}