package com.theromman.aggregators

import com.theromman.model.ProductInfo
import com.theromman.model.ProductInfoExtended
import java.lang.RuntimeException

class ProductPurchasingsAggregatorDefault : ProductPurchasingsAggregator {
    override fun aggregate(
        products: List<String>,
        productsMappings: Map<String, ProductInfo>
    ): Iterable<ProductInfoExtended> {
        checkIsAllMappingsExist(products, productsMappings)
        val productToQuantity = products.groupingBy { it }.eachCount()
        return productToQuantity.map {
            val mappingOfProduct = productsMappings[it.key]
            ProductInfoExtended(
                productInfo = ProductInfo(version = mappingOfProduct!!.version, edition = mappingOfProduct.edition),
                quantity = it.value
            )
        }
    }

    private fun checkIsAllMappingsExist(products: List<String>, productsMappings: Map<String, ProductInfo>) {
        val productsWithoutMappings = products.filter { it !in productsMappings }.toSet()
        if (productsWithoutMappings.isNotEmpty()) {
            throw RuntimeException("Not found mapping for products: $productsWithoutMappings")
        }
    }
}