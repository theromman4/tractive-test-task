package com.theromman.model

data class ProductInfo(
    val version: Int,
    val edition: String? = null
)