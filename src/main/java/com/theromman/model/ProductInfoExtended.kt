package com.theromman.model

data class ProductInfoExtended(
    val productInfo: ProductInfo,
    val quantity: Int
)