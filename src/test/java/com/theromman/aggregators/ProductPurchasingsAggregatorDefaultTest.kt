package com.theromman.aggregators

import com.theromman.model.ProductInfoExtended
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import utils.builders.com.theromman.model.ProductInfoBuilder.buildProductInfo
import utils.builders.com.theromman.model.ProductInfoExtendedBuilder.buildProductInfoExtended
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ProductPurchasingsAggregatorDefaultTest {
    private val productPurchasingsAggregatorDefault = ProductPurchasingsAggregatorDefault()

    @Test
    fun `when products and mappings passed should aggregate them`() {
        val expected = createAggregatedPurchasedItems()

        val actual = productPurchasingsAggregatorDefault.aggregate(products, productsMappings)

        assertTrue { actual.toList().containsAll(expected.toList()) }
        assertTrue { expected.toList().containsAll(actual.toList()) }
    }

    @Test
    fun `when not exist mapping for product should throw exception`() {
        val expectedMessage = "Not found mapping for products: ${listOf(CVCD_SKU, SDFD_SKU, DDDF_SKU)}"

        val actualMessage = assertThrows<RuntimeException> {
            productPurchasingsAggregatorDefault.aggregate(products, emptyMap())
        }.message

        assertEquals(expectedMessage, actualMessage)
    }

    private fun createAggregatedPurchasedItems(): Iterable<ProductInfoExtended> {
        return listOf(
            buildProductInfoExtended(
                productInfo = productInfo1,
                quantity = 1
            ),
            buildProductInfoExtended(
                productInfo = productInfo2,
                quantity = 2
            ),
            buildProductInfoExtended(
                productInfo = productInfo3,
                quantity = 1
            )
        )
    }

    companion object {
        private const val CVCD_SKU = "CVCD"
        private const val SDFD_SKU = "SDFD"
        private const val DDDF_SKU = "DDDF"
        private val products = listOf(
            CVCD_SKU,
            SDFD_SKU,
            DDDF_SKU,
            SDFD_SKU
        )
        private val productInfo1 = buildProductInfo(
            version = 1,
            edition = "X"
        )
        private val productInfo2 = buildProductInfo(version = 2, edition = "Z")
        private val productInfo3 = buildProductInfo(version = 1)
        private val productsMappings = mapOf(
            CVCD_SKU to productInfo1,
            SDFD_SKU to productInfo2,
            DDDF_SKU to productInfo3
        )
    }
}