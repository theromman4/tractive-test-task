package utils.builders.com.theromman.model

import com.theromman.model.ProductInfo

object ProductInfoBuilder {
    fun buildProductInfo(
        version: Int = 1,
        edition: String? = null
    ): ProductInfo {
        return ProductInfo(
            version = version,
            edition = edition
        )
    }
}