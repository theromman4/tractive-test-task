package utils.builders.com.theromman.model

import com.theromman.model.ProductInfo
import com.theromman.model.ProductInfoExtended
import utils.builders.com.theromman.model.ProductInfoBuilder.buildProductInfo

object ProductInfoExtendedBuilder {
    fun buildProductInfoExtended(
        productInfo: ProductInfo = buildProductInfo(),
        quantity: Int = 0
    ): ProductInfoExtended {
        return ProductInfoExtended(
            productInfo = productInfo,
            quantity = quantity
        )
    }
}