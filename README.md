# SKU, Version, Quantity - Coding Task



## Task as is

Write a method that takes two inputs: a list of purchased product codes and a map of
mappings for these codes. The method should return an aggregated list of purchased
products and quantity based on the list of purchased products codes.<br>
**Inputs**<br>
List of products: <br>
```["CVCD", "SDFD", "DDDF", "SDFD"]``` <br>
Mappings: <br>
```
{
  "CVCD": {
    "version": 1,
    "edition": "X"
  },
  "SDFD": {
    "version": 2,
    "edition": "Z"
  },
  "DDDF": {
    "version": 1
  }
}
```

**Expected Output** <br>
Purchased items: <br>
```
[
   {
      "version":1,
      "edition":"X",
      "quantity":1
   },
   {
      "version":1,
      "quantity":1
   },
   {
      "version":2,
      "edition":"Z",
      "quantity":2
   }
]
```

## See result
To see result you should run only one test - ```ProductPurchasingsAggregatorDefaultTest```, all test data is provided there